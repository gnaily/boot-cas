# boot-cas

#### 项目介绍
cas5.3.4学习使用，使用Overlay生成

#### 软件架构
软件架构说明

一.cas-server-base 单点登录-骨架搭建
二.cas-server-jdbc JDBC认证(密码MD5和密码加盐)
    参考文档:
       &nbsp;  [CAS属性](https://apereo.github.io/cas/5.3.x/installation/Configuration-Properties.html#ldap-authentication-1)
       &nbsp;  [CAS共同属性](https://apereo.github.io/cas/5.3.x/installation/Configuration-Properties-Common.html)
三.cas-server-custom-verifify 自定义密码认证
    [CAS共同属性](https://apereo.github.io/cas/5.3.x/installation/Configuration-Properties-Common.html)
四.cas-server-custom-verifify2 自定义登录认证

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. [使用https服务证书生成参考](https://blog.csdn.net/zl_jay/article/details/10006157)
2. 在使用客户端实现单点登录时必须把生成的证书导入到jdk中，命令如下
     -import -file 你的证书的位置\server.cer -keystore "%JAVA_HOME%\jre\lib\security\cacerts" -alias server -storepass changeit（changeit为默认密码）
    如果不导入登录会报Keystore was tampered with, or password was incorrect错误
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)