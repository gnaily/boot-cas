package com.cas.exception;

import javax.security.auth.login.AccountException;

/**
 * 用户没找到异常
 * @author Leighton
 * @create 2018-11-11.
 */
public class MyAccountNotFoundException extends AccountException {

    public MyAccountNotFoundException() {
        super();
    }

    public MyAccountNotFoundException(String msg) {
        super(msg);
    }
}
