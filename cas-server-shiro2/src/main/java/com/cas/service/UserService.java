package com.cas.service;

import java.util.Map;

/**
 * @author Leighton
 * @create 2018-11-09.
 */
public interface UserService {
    Map<String,Object> findByUserName(String userName);
}
